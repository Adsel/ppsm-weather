package com.example.weatherapp.httpclient;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.example.weatherapp.MainActivity;
import com.example.weatherapp.WeatherActivity;
import com.example.weatherapp.model.Weather;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class HttpClient {
    private Retrofit retrofit;
    private JsonPlaceholderAPI jsonPlaceholderAPI;
    private String path = "https://api.openweathermap.org/data/2.5/";
    private WeatherActivity weatherActivity;
    private MainActivity mainActivity;
    private char whichActivity;



    public HttpClient(MainActivity m){
        retrofit = new Retrofit.Builder()
                .baseUrl(this.path)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        jsonPlaceholderAPI = retrofit.create(JsonPlaceholderAPI.class);
        this.mainActivity = m;
        whichActivity = 'm';
    }

    public HttpClient(){
        retrofit = new Retrofit.Builder()
                .baseUrl(this.path)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        jsonPlaceholderAPI = retrofit.create(JsonPlaceholderAPI.class);
    }

    public HttpClient(WeatherActivity w){
        retrofit = new Retrofit.Builder()
                .baseUrl(this.path)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        jsonPlaceholderAPI = retrofit.create(JsonPlaceholderAPI.class);
        this.weatherActivity = w;
        whichActivity = 'w';
    }

    public void getWeather(String locationName){
        String city = removePolishChars(locationName);
        Call<Weather> call = jsonPlaceholderAPI.getWeather(
                city + ",pl",
                "749561a315b14523a8f5f1ef95e45864",
                "metric"
        );

        call.enqueue(new Callback<Weather>() {
            @Override
            public void onResponse(Call<Weather> call, Response<Weather> response) {
                if (!response.isSuccessful()){
                    System.out.println("BLAD");
                    return;
                }
                Weather weather = response.body();
                try {
                    weatherActivity.printWeather(weather);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<Weather> call, Throwable t) {
                System.out.println("BLAD 2");
            }
        });
    }

    public void checkName(String locationName){
        String city = removePolishChars(locationName);
        Call<Weather> call = jsonPlaceholderAPI.getWeather(
                city + ",pl",
                "749561a315b14523a8f5f1ef95e45864",
                "metric"
        );

        call.enqueue(new Callback<Weather>() {
            @Override
            public void onResponse(Call<Weather> call, Response<Weather> response) {
                if (!response.isSuccessful()){
                    mainActivity.getToast().show();
                    return;
                }
                System.out.println(response.code() + " KOD ODPO");
                mainActivity.goToChild();
            }

            @Override
            public void onFailure(Call<Weather> call, Throwable t) {
                System.out.println("Zla nazwa miejscowosci!");
            }
        });
    }

    private String removePolishChars(String input){
        input = input.replace("ł","l").replace("ą", "a").replace("ć", "c").replace("ę","e");
        input = input.replace("ż", "z").replace("ź","z").replace("ó","o").replace("ń","n");

        return input.replace("ś","s");
    }

    public boolean canRefresh(){
        ConnectivityManager connection;
        if(whichActivity == 'w'){
             connection = (ConnectivityManager) weatherActivity.getSystemService(Context.CONNECTIVITY_SERVICE);
        }
        else if(whichActivity == 'm'){
            connection = (ConnectivityManager) mainActivity.getSystemService(Context.CONNECTIVITY_SERVICE);
        }
        else{
            return false;
        }
            NetworkInfo networkInfo = connection.getActiveNetworkInfo();
        return (networkInfo != null && networkInfo.isConnected());
    }
}
