package com.example.weatherapp.httpclient;

import com.example.weatherapp.model.Weather;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface JsonPlaceholderAPI {
    @GET("weather")//?q={location},pl&APPID=749561a315b14523a8f5f1ef95e45864&units=metric")
    Call<Weather> getWeather(
            @Query("q") String q,
            @Query("APPID") String APPID,
            @Query("units") String units
    );
}
