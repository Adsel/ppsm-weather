package com.example.weatherapp;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.weatherapp.httpclient.HttpClient;

public class MainActivity extends AppCompatActivity {
    Button saveLocationBtn;
    TextView locationTxtView;
    HttpClient httpClient;
    String location;
    Toast toast;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if(loadLocation().equals("empty")) {
            saveLocationBtn = findViewById(R.id.button);
            locationTxtView = findViewById(R.id.location);
            httpClient = new HttpClient(this);
            saveLocationBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(httpClient.canRefresh()){
                        location = locationTxtView.getText().toString();
                        httpClient.checkName(location);
                    }
                    else{
                        Toast.makeText(getApplicationContext(), "Connection is lost", Toast.LENGTH_SHORT).show();
                    }
                }
            });
            toast = Toast.makeText(getApplicationContext(),"Unsuccessful",Toast.LENGTH_SHORT);
        }
        else{
            location = loadLocation();
            goToChild();
        }
    }

    public void goToChild(){
        saveLocation(location);
        Intent intent = new Intent(this, WeatherActivity.class);
        intent.putExtra("Name", location);
        startActivity(intent);
    }

    public Toast getToast(){
        return this.toast;
    }

    private void saveLocation(String output){
        SharedPreferences sharedPreferences = getSharedPreferences("shared preferences", MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("LOCATION", output);
        editor.apply();
    }

    public String loadLocation() {
        SharedPreferences sharedPreferences = getSharedPreferences("shared preferences", MODE_PRIVATE);
        String date = sharedPreferences.getString("LOCATION", "empty");

        return date;
    }
}
