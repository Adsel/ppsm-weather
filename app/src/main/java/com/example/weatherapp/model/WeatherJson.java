package com.example.weatherapp.model;

public class WeatherJson {
    Integer id;
    String main;
    String description;
    String icon;

    public String getIcon(){ return this.icon; }
}
