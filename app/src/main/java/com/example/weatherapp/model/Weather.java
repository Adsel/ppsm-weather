package com.example.weatherapp.model;

import java.util.Map;

public class Weather {
    Map<String, Double> coord;
    private WeatherJson[] weather;
    String base;
    private MainJson main;
    Integer visibility;
    WindJson wind;
    CloudsJson clouds;
    Integer dt;
    SysJson sys;
    Integer timezone;
    Integer id;
    String name;
    Integer cod;

    public MainJson getMain(){
        return this.main;
    }

    public WeatherJson[] getWeather(){ return this.weather; }
}
