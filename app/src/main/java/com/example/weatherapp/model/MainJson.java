package com.example.weatherapp.model;

public class MainJson {
    private Double temp;
    private Double feels_like;
    private Double temp_min;
    private Double temp_max;
    private Double pressure;
    private Double humidity;

    public MainJson(){}

    public Double getTemp(){
        return this.temp;
    }

    public Double getPressure(){ return this.pressure; }

    public Double getHumidity(){ return this.humidity; }

    public Double getTempMin(){ return this.temp_min; }

    public Double getTempMax(){ return this.temp_max; }
}
