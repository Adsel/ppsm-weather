package com.example.weatherapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.example.weatherapp.httpclient.DownloadImageTask;
import com.example.weatherapp.httpclient.HttpClient;
import com.example.weatherapp.model.Weather;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;

import static java.util.concurrent.TimeUnit.SECONDS;

public class WeatherActivity extends AppCompatActivity {
    private TextView temp, humidity, pressure, tempMin, tempMax, city, time;
    private String locationName;
    private ImageView image;
    private ScheduledExecutorService executor;
    private HttpClient httpClient;
    private final Integer secondsToRefresh = 60 * 5; // 5 minutes

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather);
        this.locationName = getIntent().getStringExtra("Name");

        temp = findViewById(R.id.temperature);
        humidity = findViewById(R.id.humidity);
        pressure = findViewById(R.id.pressure);
        tempMin = findViewById(R.id.tempMin);
        tempMax = findViewById(R.id.tempMax);
        city = findViewById(R.id.city);
        time = findViewById(R.id.time);
        image = findViewById(R.id.imageView);

        executor = Executors.newSingleThreadScheduledExecutor();

        httpClient = new HttpClient(this);
        httpClient.getWeather(this.locationName);

        // REFRESHING
        ((SwipeRefreshLayout) findViewById(R.id.container)).setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if(httpClient.canRefresh()) {
                    Toast.makeText(getApplicationContext(), "Manually refreshing", Toast.LENGTH_SHORT).show();
                    httpClient.getWeather(locationName);
                    ((SwipeRefreshLayout) findViewById(R.id.container)).setRefreshing(false);
                }
                else{
                    Toast.makeText(getApplicationContext(), "Can't refresh without Internet connection", Toast.LENGTH_SHORT);
                }
            }
        });
        activateAutomaticRefresh();
    }

    public void activateAutomaticRefresh(){
        final Runnable refresher = new Runnable() {
            @Override
            public void run() {
                if(httpClient.canRefresh()){
                    httpClient.getWeather(locationName);
                    System.out.println("Odswiezono");
                }
                else{
                    System.out.println("Brak polaczenia");
                }
            }
        };
        final ScheduledFuture<?> handle = executor.scheduleAtFixedRate(refresher, 2, 10, SECONDS);
    }

    private String getTimeString(){
        Date currTime = Calendar.getInstance().getTime();
        SimpleDateFormat dateF = new SimpleDateFormat("hh:mm");

        return dateF.format(currTime);
    }

    public void printWeather(Weather w) throws IOException {
        city.setText(this.locationName);
        time.setText(this.getTimeString());
        temp.setText(w.getMain().getTemp().toString() + " ℃");
        pressure.setText(w.getMain().getPressure().toString() + " hPa");
        humidity.setText(w.getMain().getHumidity().toString() + " %");
        tempMax.setText(w.getMain().getTempMax().toString() + " ℃");
        tempMin.setText(w.getMain().getTempMin().toString() + " ℃");

        new DownloadImageTask((ImageView) findViewById(R.id.image))
                .execute("https://openweathermap.org/img/wn/" + (w.getWeather())[0].getIcon() +"@2x.png");

    }
}
